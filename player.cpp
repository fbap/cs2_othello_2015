#include "player.h"
#include <stdlib.h> 

// Note: Alex edited this document as instructed by the assignment
// Comment for testing git

void printVector(std::vector<Move*> moves)
{
	for (int i = 0; i < int(moves.size()); i++)
	{
		std::cerr << "x: " << moves[i]->x << ", y: " << moves[i]->y << std::endl;
	}
	return;
}

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

	board = new Board();
	color = side;
    depth = DEPTH; // this will be modified from testminimax.cpp if that's called

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	int time_begin =  time(NULL);
	// Do opponent's move	
	if (opponentsMove)
	{
		std::cerr << "opponent's move: x :" << opponentsMove->x << ", y: " << opponentsMove->y << std::endl;
	}
	Side other = (color == BLACK) ? WHITE : BLACK;

	this->board->doMove(opponentsMove, Side(other));

	// FOR TESTING ONLY
//	std::bitset<64> newBlack (std::string("0000000000000000000001000000110000011111000001000000010000000000"));
//	std::bitset<64> newWhite (std::string("0000000000000000000001000001110000011111000001000000010000000000"));
//	board->setBoardBits(newBlack, newWhite);
	//board->find_permanent(newBlack);
	//board->find_permanent(newWhite);

	if (!board->hasMoves(color))
	{
		std::cerr << "no moves on the board" << std::endl;
		return NULL;
	}

	bitset<64> black_now = this->board->getBlack();
    bitset<64> taken_now = this->board->getTaken();

	std::cerr << "\nin player.cpp: permanent positions held by:" << std::endl;
	std::cerr << "black: " << board->find_permanent(board->black) << std::endl;
	std::cerr << "white: " << board->find_permanent(board->black ^ board->taken) << std::endl << std::endl;

    std::cerr << "CURRENT BOARD: " << std::endl;
    std::cerr << "bitset black: " << this->board->getBlack() << std::endl;
    std::cerr << "bitset taken: " << this->board->getTaken() << std::endl;



    std::vector<Move*> myMoves = board->getMoves(color);
    //printVector(myMoves);
    int best_ind = 0;
    int best_score;


    int best_inds[3] = {0,0,0};
    int best_scores[3];

	if(color == WHITE)
	{
		best_scores[0] = -INFINITY;
		best_scores[1] = -INFINITY;
		best_scores[2] = -INFINITY;
		
		for (int i = 0; i < int(myMoves.size()); i++)
		{
			board->doMove(myMoves[i], Side(color));  // do a move
			int score = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth, best_scores[0], INFINITY);
			
			if (score > best_scores[0]) // Best score
			{
				best_scores[2] = best_scores[1];
				best_inds[2] = best_inds[1];

				best_scores[1] = best_scores[0];
				best_inds[1] = best_inds[0];
				
				best_scores[0] = score;
				best_inds[0] = i;				
			}
			else if (score > best_scores[1]) // Second-best score
			{
				best_scores[2] = best_scores[1];
				best_inds[2] = best_inds[1];

				best_scores[1] = score;
				best_inds[1] = i;
			}
			else if (score > best_scores[2])  // Third-best score
			{
				best_scores[2] = score;
				best_inds[2] = i;
			}
			board->setBoardBits(black_now, taken_now); // undo move
		}
		int score1 = best_scores[0];
		int score2 = best_scores[1];
		int score3 = best_scores[2];
		std::cerr << "time begin" << time_begin << std::endl;
		std::cerr << "time noe" << time(NULL) << std::endl;
		std::cerr << "time left" << msLeft << std::endl;

			
		int movesLeft = 64 - board->getTaken().count() - 5; // last ~5 moves are really fast
		// still have some time
		if ((msLeft / 1000. - (time_begin - time(NULL))) / TOTAL_MS < 1.0 / movesLeft) 
		{
			std::cerr << "deepening to " << depth+3 << std::endl;
			score1 = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth+3, best_scores[0], INFINITY);
		}
		if ((msLeft / 1000. - (time_begin - time(NULL))) / TOTAL_MS < 1.0 / movesLeft) 
		{
			score2 = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth+3, best_scores[1], INFINITY);
		}
		if ((msLeft / 1000. - (time_begin - time(NULL))) / TOTAL_MS < 1.0 / movesLeft) 
		{
			score3 = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth+3, best_scores[2], INFINITY);
		}
		
		if (score1 > score2)
		{
			if (score1 > score3)
				best_ind = 1;
			else
				best_ind = 3;
		}
		else if (score2 > score3)
		{
			best_ind = 2;
		}
		else
		{
			best_ind = 3;
		}
	}
    else if (color == BLACK)
	{
		best_scores[0] = INFINITY;
		for (int i = 0; i < int(myMoves.size()); i++)
		{
			board->doMove(myMoves[i], color);
			int score = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth, -INFINITY, best_scores[0]);
			
			if (score < best_scores[0]) // Best score
			{
				best_scores[2] = best_scores[1];
				best_inds[2] = best_inds[1];

				best_scores[1] = best_scores[0];
				best_inds[1] = best_inds[0];
				
				best_scores[0] = score;
				best_inds[0] = i;				
			}
			else if (score < best_scores[1]) // Second-best score
			{
				best_scores[2] = best_scores[1];
				best_inds[2] = best_inds[1];

				best_scores[1] = score;
				best_inds[1] = i;
			}
			else if (score < best_scores[2])  // Third-best score
			{
				best_scores[2] = score;
				best_inds[2] = i;
			}
			
			board->setBoardBits(black_now, taken_now); // undo move
		}

		std::cerr << "deepening to " << depth+3 << std::endl;		
		int score1 = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth+3, best_scores[0], INFINITY);
		int score2 = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth+3, best_scores[1], INFINITY);
		int score3 = board->getMinimaxMoves(board->getBlack(), board->getTaken(), other, depth+3, best_scores[2], INFINITY);
		std::cerr << "scores: " << score1 << std::endl;
		//std:cerr << score2 << score3 << std::endl;	
		/*if (score1 < score2)
		{
			if (score1 < score3)
				best_ind = 1;
			else
				best_ind = 3;
		}
		else if (score2 < score3)
			best_ind = 2;
		else
			best_ind = 3;
*/
	}
	
	this->board->record_move(best_score, -INFINITY, INFINITY, black_now, taken_now, color);


	// restore board position
	board->setBoardBits(black_now, taken_now);
	

   // Do the best move 
    board->doMove(myMoves[best_ind], Side(color));
   	std::cerr << "best score: " << best_score << std::endl;
   	std::cerr << "actual score: " << board->getScore(board->getBlack(), board->getTaken()) << std::endl;
    std::cerr << "my move" << myMoves[best_ind]->getX() << myMoves[best_ind]->getY() << std::endl;



	
    this->board->sendSequence(this->color);
    Move * returnMove = new Move(myMoves[best_ind]->getX(), myMoves[best_ind]->getY());
    this->board->delete_move_vector(myMoves);
	return returnMove;
}
