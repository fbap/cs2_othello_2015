#include "board.h"
#include <vector>
#include "player.h"
#include <stdio.h>
#include <string>
#include <stdlib.h> 
/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
	taken.set(3 + 8 * 3);
	taken.set(3 + 8 * 4);
	taken.set(4 + 8 * 3);
	taken.set(4 + 8 * 4);
	black.set(4 + 8 * 3);
	black.set(3 + 8 * 4);


	cornerWeight = DEFAULT_CORNER_WEIGHT;
	diagonalToCornerWeight = DEFAULT_DIAGONAL_TO_CORNER_WEIGHT;
	inlineWithCornerWeight = DEFAULT_INLINE_WITH_CORNER_WEIGHT;
	row2Weight = DEFAULT_ROW_3_WEIGHT;
	row3Weight = DEFAULT_ROW_2_WEIGHT;
	row4Weight = DEFAULT_ROW_4_WEIGHT;
	edgeWeight = DEFAULT_EDGE_WEIGHT;
	permPosWeight = DEFAULT_PERM_POS_WEIGHT;
	openMoveWeight = DEFAULT_OPEN_MOVE_WEIGHT;



	PermPosInfo * topLeft = new PermPosInfo(0, TOP, LEFT);
	topLeft->possiblePositions.insert(1);
	topLeft->possiblePositions.insert(8);

	PermPosInfo * topRight = new PermPosInfo(7, TOP, RIGHT);
	topRight->possiblePositions.insert(6);
	topRight->possiblePositions.insert(15);

	PermPosInfo * bottomLeft = new PermPosInfo(56, BOTTOM, LEFT);
	bottomLeft->possiblePositions.insert(48);
	bottomLeft->possiblePositions.insert(57);

	PermPosInfo * bottomRight = new PermPosInfo(63, BOTTOM, RIGHT);
	bottomRight->possiblePositions.insert(55);
	bottomRight->possiblePositions.insert(62);


	PermPos.push_back(topLeft);
	PermPos.push_back(topRight);
	PermPos.push_back(bottomLeft);
	PermPos.push_back(bottomRight);

	this->updateParams(); // update the parameters from the parameter file;



}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
	Board *newBoard = new Board();
	newBoard->black = black;
	newBoard->taken = taken;
	newBoard->cornerWeight = cornerWeight;
	newBoard->diagonalToCornerWeight = diagonalToCornerWeight;
	newBoard->inlineWithCornerWeight = inlineWithCornerWeight;
	newBoard->row2Weight = row2Weight;
	newBoard->row3Weight = row3Weight;
	newBoard->row4Weight = row4Weight;
	newBoard->edgeWeight = edgeWeight;
	newBoard->permPosWeight = permPosWeight;
	newBoard->PermPos = PermPos;
	newBoard->openMoveWeight = openMoveWeight;


	return newBoard;
}

bool Board::occupied(int x, int y) {
	return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
	return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
	taken.set(x + 8*y);
	black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
	return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
	if (!(hasMoves(BLACK) || hasMoves(WHITE))) // game is done. print stats
	{
		FILE * pFile = fopen ("Simpleplayer.txt","w");
		fprintf (pFile, "%d\n",this->countWhite() - this->countWhite());
		fclose(pFile);
		
	}
	return !(hasMoves(BLACK) || hasMoves(WHITE));

	 
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			Move move(i, j);
			if (checkMove(&move, side)) 
			{
				return true;
			}
		}	
	}
	return false;
}

/*
 * Returns all possible moves for the given side.
 */
std::vector<Move*> Board::getMoves(Side side) {
	std::vector<Move*> moves;
	
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			Move move(i, j);
			if (checkMove(&move, side)) {
				Move* new_move = new Move(i,j);
				moves.push_back(new_move);
			}
		}
	}
	return moves;
}

void Board::delete_move_vector(std::vector<Move*> vec)
{
	for (std::vector<Move*>::iterator it = vec.begin(); it != vec.end(); it++)
	{
		delete *it;
	}
	vec.clear();
	vector<Move*>().swap(vec);
}



void Board::setBoardBits(bitset<64> new_black, bitset<64> new_taken) {
	this->black = new_black;
	this->taken = new_taken;
}

/*
 * Returns all possible moves for the given side.
 */
int Board::getScore(bitset<64> black, bitset<64> taken) {

	//return countWhite() - countBlack();
	bitset<64> white = taken ^ black;
	
	//std::cerr << "black" << black << std::endl;
	//std:: cerr <<"\ntaken" << taken << std::endl;
	//std::cerr << "white" << white << std::endl;
	
	
	// 
	
	int score = 0;

	
	bitset<64> overlap1(CORNER_BITS);
	//std::cerr << overlap1 << std::endl;
	overlap1 &= white;
	score += overlap1.count() * cornerWeight;
	//std::cout << score << std::endl;
	//std::cout << overlap1.count();

		
		
	bitset<64> overlap2(DIAGONAL_TO_CORNER_BITS);
	overlap2 &= white;
	score += overlap2.count() * diagonalToCornerWeight;

	bitset<64> overlap3(INLINE_WITH_CORNER_BITS);
	overlap3 &= white;
	score += overlap3.count() * inlineWithCornerWeight;

	bitset<64> overlap4(ROW_2_BITS);
	overlap4 &= white;
	score += overlap4.count() * row2Weight;

		//std::cout << "score" << score << std::endl;
	
		bitset<64> overlap5(ROW_3_BITS);
		overlap5 &= white;
		score += overlap5.count() * row3Weight;

		bitset<64> overlap6(ROW_4_BITS);
		overlap6 &= white;
		score += overlap6.count() * row4Weight;

		bitset<64> overlap7(EDGE_BITS);
		overlap7 &= white;
		score += overlap7.count() * edgeWeight;


		// For black
		bitset<64> bOverlap1(CORNER_BITS);
		bOverlap1 &= black;
		score -= bOverlap1.count() * cornerWeight;

		bitset<64> bOverlap2(DIAGONAL_TO_CORNER_BITS);
		bOverlap2 &= black;
		score -= bOverlap2.count() * diagonalToCornerWeight;

		bitset<64> bOverlap3(INLINE_WITH_CORNER_BITS);
		bOverlap3 &= black;
		score -= bOverlap3.count() * inlineWithCornerWeight;

		bitset<64> bOverlap4(ROW_2_BITS);
		bOverlap4 &= black;
		score -= bOverlap4.count() * row2Weight;

		bitset<64> bOverlap5(ROW_3_BITS);
		bOverlap5 &= black;
		score -= bOverlap5.count() * row3Weight;

		bitset<64> bOverlap6(ROW_4_BITS);
		bOverlap6 &= black;
		score -= bOverlap6.count() * row4Weight;

		bitset<64> bOverlap7(EDGE_BITS);
		bOverlap7 &= black;
		score -= bOverlap7.count() * edgeWeight;
	
	
	
	
	// find the number of permanent positions held by each team and add it to the score
	score += find_permanent(white) * permPosWeight;
	score -= find_permanent(black) * permPosWeight;

	// find the number of possible moves in the board for each team (more is better) and add to score
	vector<Move*> whiteMoves = getMoves(WHITE);
	vector<Move*> blackMoves = getMoves(BLACK);
	score += whiteMoves.size() * openMoveWeight;
	score -= blackMoves.size() * openMoveWeight;
	delete_move_vector(whiteMoves);
	delete_move_vector(blackMoves);

	return score;
}

// check to see if the board space cand is in PermPos
bool Board::in_PermPos(int cand)
{
	for (std::vector<PermPosInfo*>::iterator it = this->PermPos.begin(); it != this->PermPos.end(); it++)
	{
		if ((*it)->pos == cand)
		{
			return true;
		}
	}
	return false;
}

int Board::find_permanent(std::bitset<64> side)
{
	//std::cerr << "find_permanent called" << std::endl;
	int score = 0;
	int myIt = 0;
	// std::vector<PermPosInfo*>::iterator itPerm = PermPos.begin();
	int endSize = PermPos.size();
	while (1) // (myIt != (PermPos.size() - 1)) // make sure to update myIt in loop each time
	{
		if (myIt >= endSize)
		{
			break;
		}
		PermPosInfo * itPermRef = PermPos[myIt];
		if (!side.test((itPermRef)->pos))
		{
			myIt++;
			continue;
		}
		score++;
		for (std::set<int>::iterator itPos = (itPermRef)->possiblePositions.begin(); itPos != (itPermRef)->possiblePositions.end(); itPos ++)
		{
			if (side.test(*itPos) && (onEdgeOrIn(side, *itPos, (itPermRef)->vert) && onEdgeOrIn(side, *itPos, (itPermRef)->horiz)))
			{
				if (in_PermPos(*itPos))
				{
					continue;
				}
				PermPosInfo * newPermPosInfo = new PermPosInfo(*itPos, (itPermRef)->vert, (itPermRef)->horiz);
				if ((itPermRef)->vert == TOP)
				{
					if (!(*itPos >= 56))
					{
						newPermPosInfo->possiblePositions.insert(*itPos + 8);
					}
				}
				else
				{
					if(!(*itPos <= 7))
					{
						//newPermPosInfo->possiblePositions.insert(*itPos - 8);
					}
				}

				if ((itPermRef)->horiz == LEFT)
				{
					if(!(*itPos % 8 == 7))
					{
						newPermPosInfo->possiblePositions.insert(*itPos + 1);
					}
				}
				else
				{
					if(!(*itPos % 8 == 0))
					{
						newPermPosInfo->possiblePositions.insert(*itPos - 1);
					}
				}
				PermPos.push_back(newPermPosInfo);
				endSize++;
			}
			//std::cerr << "board.cpp: check 2" << std::endl;	
		}
		myIt++;
	}
	for (std::vector<PermPosInfo*>::iterator it = PermPos.begin() + 4; it != PermPos.end(); it++)
	{
		delete *it;
	}
	PermPos.erase(PermPos.begin() + 4, PermPos.end());

	return score;
}

// check to see if the candidate has an edge or another piece of the same side in PermPos, return true if this is the case
bool Board::onEdgeOrIn(std::bitset<64> side, int cand, Edge edge)
{
	//std::cerr << "onEdgeOrIn called" << std::endl;
	if (edge == TOP)
	{
		if (cand >= 0 && cand <= 7)
		{
			return true;
		}
		if (in_PermPos(cand - 8))
		{
			return true;
		}
		return false;
	}
	if (edge == BOTTOM)
	{
		if (cand >= 56 && cand <= 63)
		{
			return true;
		}
		if (in_PermPos(cand + 8))
		{
			return true;
		}
		return false;
	}
	if (edge == LEFT)
	{
		if (cand % 8 == 0)
		{
			return true;
		}
		if (in_PermPos(cand - 1))
		{
			return true;
		}
		return false;
	}
	if (edge == RIGHT)
	{
		if (cand % 8 == 7)
		{
			return true;
		}
		if (in_PermPos(cand + 1))
		{
			return true;
		}
		return false;
	}
	std::cerr << "An error has occured in onEdgeOrIn in Board.cpp" << std::endl;
	std::cerr << "The edge argument in onEdgeOrIn is: " << edge << std::endl;
	return false;
}


bitset<64> Board::getTaken() {
	return this->taken;
}
bitset<64> Board::getBlack() {
	return this->black;
}
/*
 * Returns a move for the given side. Based on https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning#Pseudocode
 */
int Board::getMinimaxMoves(bitset<64> black_now, bitset<64> taken_now, Side side, int depth, int alpha, int beta) {
	// it might be a good idea to update the state of the board with black_now and taken_now so we know the boards in the correct position--though this might take too long. Ideas?
	//bitset<64> black_now = black;
	//bitset<64> taken_now = taken;
	
	//Move* best_move = new Move(-1,-1);

	int checkResult = check_books(black, black ^ taken, alpha, beta, side);
	if (checkResult != NO_MATCH)
	{
		return checkResult;
	}

	int v;
	
	std::vector<Move*> moves = this->getMoves(side);
	if (!hasMoves(side) || depth <= 0) { // terminal node or final depth reached		
		delete_move_vector(moves);
		return this->getScore(black_now, taken_now);
	}
	Side other = (side == BLACK) ? WHITE : BLACK;
	if (side == WHITE) // maximizing player
	{
		v = -INFINITY;
		for (int i = 0; i < int(moves.size()); i++)
		{
			this->doMove(moves[i], side);
			v = max(v, this->getMinimaxMoves( this->getBlack(), this->getTaken(), other, depth-1, alpha, beta));
			alpha = max(alpha, v);			
			this->setBoardBits(black_now, taken_now); // undo move
			
			if (beta <= alpha)
			{
				delete_move_vector(moves);
				record_move(v, alpha, beta, black_now, taken_now, side);
				return v;
			}
		}
		delete_move_vector(moves);
		record_move(v, alpha, beta, black_now, taken_now, side);
		return v;
		
	}
	else // minimizing player (side == BLACK)
	{
		v = INFINITY;
		for (int i = 0; i < int(moves.size()); i++)
		{
			this->doMove(moves[i], side);
			
			v = min(v, this->getMinimaxMoves( this->getBlack(), this->getTaken(), other, depth-1, alpha, beta));
			beta = min(beta, v);
			this->setBoardBits(black_now, taken_now); // undo move
				
			if (beta <= alpha)
			{
				delete_move_vector(moves);
				record_move(v, alpha, beta, black_now, taken_now, side);
				return v;
			}
			
		}
		delete_move_vector(moves);
		record_move(v, alpha, beta, black_now, taken_now, side);
		return v;
	}		
	
	
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
	// Passing is only legal if you have no moves.
	if (m == NULL) return !hasMoves(side);

	int X = m->getX();
	int Y = m->getY();

	// Make sure the square hasn't already been taken.
	if (occupied(X, Y)) return false;

	Side other = (side == BLACK) ? WHITE : BLACK;
	for (int dx = -1; dx <= 1; dx++) {
		for (int dy = -1; dy <= 1; dy++) {
			if (dy == 0 && dx == 0) continue;

			// Is there a capture in that direction?
			int x = X + dx;
			int y = Y + dy;
			if (onBoard(x, y) && get(other, x, y)) {
				do {
					x += dx;
					y += dy;
				} while (onBoard(x, y) && get(other, x, y));

				if (onBoard(x, y) && get(side, x, y)) 
				{
					return true;
				}
			}
		}
	}
	return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
	// A NULL move means pass.
	if (m == NULL) return;

	// Ignore if move is invalid.
	if (!checkMove(m, side)) return;

	int X = m->getX();
	int Y = m->getY();
	Side other = (side == BLACK) ? WHITE : BLACK;
	for (int dx = -1; dx <= 1; dx++) {
		for (int dy = -1; dy <= 1; dy++) {
			if (dy == 0 && dx == 0) continue;

			int x = X;
			int y = Y;
			do {
				x += dx;
				y += dy;
			} while (onBoard(x, y) && get(other, x, y));

			if (onBoard(x, y) && get(side, x, y)) {
				x = X;
				y = Y;
				x += dx;
				y += dy;
				while (onBoard(x, y) && get(other, x, y)) {
					set(side, x, y);
					x += dx;
					y += dy;
				}
			}
		}
	}
	set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
	return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
	return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
	return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
	taken.reset();
	black.reset();
	for (int i = 0; i < 64; i++) {
		if (data[i] == 'b') {
			taken.set(i);
			black.set(i);
		} if (data[i] == 'w') {
			taken.set(i);
		}
	}
}

std::bitset<128> Board::combine(std::bitset<64> black, std::bitset<64> white)
{
	std::bitset<128> out (black.to_string() + white.to_string());
	return out;
}

void Board::record_move(int val, int alpha, int beta, std::bitset<64> black, std::bitset<64> white, Side side)
{
	valInfo * vi = new valInfo(val, alpha, beta, side);
	knownScore[combine(black, white)] = vi;
	return;
}

int Board::check_books(std::bitset<64> black, std::bitset<64> white, int alpha, int beta, Side side)
{
	std::unordered_map<std::bitset<128>,valInfo*>::iterator it = knownScore.find(combine(black, white));
	if (it != knownScore.end())
	{
		if ((it->second->alpha >= alpha && it->second->beta <= beta) && it->second->side == side)
		{
			return it->second->val;
		}
	}
	return NO_MATCH;
}

int Board::updateParams()
{
	std::ifstream file;
	file.open(INPUT_FILE_NAME);
	if (file.is_open())
	{
		file >> cornerWeight;
		file >> diagonalToCornerWeight;
		file >> inlineWithCornerWeight;
		file >> row2Weight;
		file >> row3Weight;
		file >> row4Weight;
		file >> edgeWeight;
		file >> permPosWeight;
		file >> openMoveWeight;
	}
	file.close();

	std::ofstream outfile;
	outfile.open(GAME_STATUS_FILE_NAME, ios::trunc);
	if (outfile.is_open())
	{
		outfile << 0; // Game in progress
	}
	outfile.close();

	return 0;
}

// call after every move, will check if the game is ending and send data to param control file accordingly
// return 0 for game not over, 1 for game over
int Board::sendSequence(Side side)
{
	if ((!(this->taken.count() == 64 || this->taken.count() == 63)) && (this->hasMoves(BLACK) || this->hasMoves(WHITE)))
	{
		return 0;
	}
	
	// update the game status
	std::ofstream statusFile;
	statusFile.open(GAME_STATUS_FILE_NAME, std::ios::trunc);
	if (statusFile.is_open())
	{
		statusFile << 1 << std::endl; // Game is complete
	}

	// update the game result

	std::ofstream outFile;
	outFile.open(OUTPUT_FILE_NAME, ios::app);
	if (outFile.is_open())
	{
		std::cerr << "game over. doing stuff..." << std::endl;
		// update the game status
		std::ofstream statusFile;
		statusFile.open(GAME_STATUS_FILE_NAME, ios::trunc);
		if (statusFile.is_open())
		{
			statusFile << 1; // Game is complete
		}

		// update the game result
		int blackScore = this->black.count();

		std::ofstream outFile;
		outFile.open(OUTPUT_FILE_NAME, ios::app);
		if (outFile.is_open())
		{
			if (side == BLACK)
			{
				if (blackScore >= 32)
				{
					outFile << "W with score: " << blackScore << std::endl;
					statusFile << 1 << std::endl; // 1 for win, 0 for loss
				}
				else
				{
					outFile << "L with score: " << blackScore << std::endl;
					statusFile << 0 << std::endl;
				}
			}
			else
			{
				int whiteScore = 64 - blackScore;
				if (whiteScore >= 32)
				{
					outFile << "W with score: " << whiteScore << std::endl;
					statusFile << 1 << std::endl;
				}
				else
				{
					outFile << "L with score: " << whiteScore << std::endl;
					statusFile << 0 << std::endl;
				}
			}
		}
		statusFile.close();
		outFile.close();
		std::cerr << "... done. the end." << std::endl;
	}
	return 0;
}


/*
 * Scores the board for the given side
*/
/*int Board::getScore(bitset<64> new_black, bitset<64> new_taken)
{
	//unsigned long long boardLong = black.to_ullong();
	
	//boardLong = taken.to_ullong() ^ boardLong;
	bitset<64> CORNER_BITS (std::string("1000000100000000000000000000000000000000000000000000000010000001"));

	bitset<64> EDGE_BITS (std::string("1111111110000001100000011000000110000001100000011000000111111111"));
	bitset<64> DIAGONAL_TO_CORNER_BITS (std::string("0000000001000010000000000000000000000000000000000100001000000000"));
	bitset<64> INLINE_WITH_CORNER_BITS (std::string("0100001010000001000000000000000000000000000000001000000101000010"));
	bitset<64> ROW_2_BITS (std::string("0000000001111110010000100100001001000010010000101111111000000000"));
	bitset<64> ROW_3_BITS (std::string("0000000000000000001111000010010000100100001111000000000000000000"));
	bitset<64> ROW_4_BITS (std::string("0000000000000000000000000001100000011000000000000000000000000000"));

	
	int score = 0;
	bitset<64> white = taken ^ black;
	bitset<64> overlap1(CORNER_BITS);
	std::cerr << overlap1 << std::endl;
	overlap1 &= white;
	std::cerr << overlap1 << std::endl;
	std::cerr << overlap1.count() << std::endl;
	score += overlap1.count() * cornerWeight;
	
	bitset<64> overlap4(ROW_2_BITS);
	overlap4 &= white;
	score += overlap4.count() * row2Weight;
	std::cerr << overlap4 << std::endl;
	std::cerr << overlap4.count() << std::endl;
	
	return 1;
}*/

