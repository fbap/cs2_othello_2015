#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
#include "Thread.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <set>
#include <unordered_map>
#include <unistd.h>


#define INPUT_FILE_NAME "params.txt"
#define GAME_STATUS_FILE_NAME "status.txt"
#define OUTPUT_FILE_NAME "result.txt"
#define NON_TURN_COMP_STATUS_FILE "ntcs.txt"
#define NO_MATCH -123456
#define INFINITY 10000000
#define WAIT_TIME_IN_BOARD 1000

#define CORNER_BITS std::string("1000000100000000000000000000000000000000000000000000000010000001")
#define EDGE_BITS std::string("0011110000000000100000011000000110000001100000010000000000111100")
#define DIAGONAL_TO_CORNER_BITS std::string("0000000001000010000000000000000000000000000000000100001000000000")
#define INLINE_WITH_CORNER_BITS std::string("0100001010000001000000000000000000000000000000001000000101000010")
#define ROW_2_BITS std::string("0000000000111100010000100100001001000010010000100011110000000000")
#define ROW_3_BITS std::string("0000000000000000001111000010010000100100001111000000000000000000")
#define ROW_4_BITS std::string("0000000000000000000000000001100000011000000000000000000000000000")

#define DEFAULT_CORNER_WEIGHT 0
#define DEFAULT_DIAGONAL_TO_CORNER_WEIGHT 0
#define DEFAULT_INLINE_WITH_CORNER_WEIGHT 0
#define DEFAULT_ROW_2_WEIGHT 0
#define DEFAULT_ROW_3_WEIGHT 0
#define DEFAULT_ROW_4_WEIGHT 0
#define DEFAULT_EDGE_WEIGHT 0
#define DEFAULT_PERM_POS_WEIGHT 1
#define DEFAULT_OPEN_MOVE_WEIGHT 0



using namespace std;

enum Edge
{
	TOP, BOTTOM, LEFT, RIGHT
};

struct PermPosInfo // for knowing which corner of the board the piece is
{
	int pos;
	Edge vert;
	Edge horiz;
	std::set<int> possiblePositions;
	PermPosInfo(int posIn, Edge vertIn, Edge horizIn)
	{
		pos = posIn;
		vert = vertIn;
		horiz = horizIn;
	}
};

struct valInfo
{
	int val;
	int alpha;
	int beta;
	Side side;
	valInfo(int valIn, int alphaIn, int betaIn, Side sideIn)
	{
		val = valIn;
		alpha = alphaIn;
		beta = betaIn;
		side = sideIn;
	}
};

struct score_move {
	int score;
	Move* move;
};

class Board {
   
private:

	
	   
	bool occupied(int x, int y);
	bool get(Side side, int x, int y);
	void set(Side side, int x, int y);
	bool onBoard(int x, int y);

	  
public:
	Board();
	~Board();
	Board *copy();

	// I moved these from private for testing purposes-- Alex
	bitset<64> black;
	bitset<64> taken;
	std::vector<PermPosInfo*> PermPos;
	std::unordered_map<std::bitset<128>,valInfo*> knownScore;
	int oppBestMove;


    int cornerWeight;
    int diagonalToCornerWeight;
    int inlineWithCornerWeight;
    int row2Weight;
    int row3Weight;
    int row4Weight;
    int edgeWeight;
    int permPosWeight;
    int openMoveWeight;

	bitset<64> getBlack();
	bitset<64> getTaken();
	void setBoardBits(bitset<64> black, bitset<64> taken);
	bool isDone();
	bool hasMoves(Side side);
	bool checkMove(Move *m, Side side);
	int getMinimaxMoves(bitset<64> black_now, bitset<64> taken_now, Side side, int depth, int alpha, int beta);
	std::vector<Move*> getMoves(Side side);
	void doMove(Move *m, Side side);
	int getScore(bitset<64> black, bitset<64> taken);
	int count(Side side);
	int countBlack();
	int countWhite();
	void addPermPos(Move *m);

	void setBoard(char data[]);
	int updateParams(); // used to update the weight parameters from an outside file
	int sendSequence(Side side);
	int find_permanent(std::bitset<64> side);
	bool onEdgeOrIn(std::bitset<64> side, int cand, Edge edge);
	bool in_PermPos(int cand);
	void delete_move_vector(std::vector<Move*> vec);
	std::bitset<128> combine(std::bitset<64> black, std::bitset<64> white);
	void record_move(int val, int alpha, int beta, std::bitset<64> black, std::bitset<64> white, Side side);
	int check_books(std::bitset<64> black, std::bitset<64> white, int alpha, int beta, Side side);
	void * nonTurnComp(void * foo);
};



#endif
